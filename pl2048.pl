#!/usr/bin/env perl
use strict;
use warnings;
use Switch;
use Term::ReadKey;

my @board = ( [ 0, 0, 0, 0 ],
              [ 0, 0, 0, 0 ],
              [ 0, 0, 0, 0 ],
              [ 0, 0, 0, 0 ] );

my @score = ( 0, 0 );

my $esc = chr( 27 )."[";

sub main {
    my $key;
    addnum();
    addnum();
    drawboard();
    ReadMode( 4 );
    MAINLOOP: while( 1 ) {
        drawstate(); 
        my @keys = ( );
        until( defined( $key = ReadKey( -1 ) ) ) {
            sleep 1;
        }
        while( defined( $key ) ) {
            push( @keys, ord( $key ) );
            $key = ReadKey( -1 );
        }
        switch( "@keys" ) {
            case [ "27 91 68", ord("a"), ord("A"),
                   ord("h"), ord ("H"), ord("4") ] {
                pushlt();
            }
            case [ "27 91 67", ord("d"), ord("D"),
                   ord("l"), ord("L"), ord("6") ] {
                pushrt();
            }
            case [ "27 91 65", ord("w"), ord("W"),
                   ord("k"), ord("K"), ord("8") ] {
                pushup();
            }
            case [ "27 91 66", ord("s"), ord("S"),
                   ord("j"), ord("J"), ord("2") ] {
                pushdn();
            }
            case [ "3", "17", "24", "26", "27", ord("q"), ord("Q"),
                   ord("x"), ord("X"), ord(".") ] {
                last MAINLOOP;
            }
        }
    }
    ReadMode('restore');
    print loc( 1, 18 );
}

sub loc {
    my ( $locc, $locr ) = @_;
    return $esc.$locr.";".$locc."H";
}

sub drawboard {
    my @bd = ( "+-------+-------+-------+-------+",
               "|       |       |       |       |" );
    print $esc."2J";
    print loc( 4, 2 )."Score:";
    print loc( 4, 3 )."Turns:";
    for( my $r = 0; $r < 9; $r++ ) {
        print loc( 4, 5+$r ).$bd[$r%2];
    }
    print loc( 4, 15 )."Controls: WASD, HJKL, 8426, Arrows";
    print loc( 4, 16 )."Quit: Esc, ^C, ^Q, ^X, ^Z, Q, X, .";
}

sub drawstate {
    my $clear = "       ";
    print loc( 11, 2 ).$score[0];
    print loc( 11, 3 ).$score[1];
    for( my $r = 0; $r < 4; $r++ ) {
        for( my $c = 0; $c < 4; $c++ ) {
            print loc( 5+(8*$c), 6+(2*$r) ).$clear;
            if( $board[$r][$c] ) {
                print loc( 5+(8*$c), 6+(2*$r) ).$board[$r][$c];
            }
        }
    }
    print loc( 1, 18 );
}

sub addnum {
    my @empty = ( );
    my ( $count, $pos, $n );
    for( my $r = 0; $r < 4; $r++ ) {
        for( my $c = 0; $c < 4; $c++ ) {
            if( $board[$r][$c] == 0 ) {
                my @t = ( $r,  $c );
                push( @empty, \@t );
            }
        }
    }
    $count = scalar( @empty );
    if ( $count > 0 ) {
        $pos = int( rand( $count ) );
        $n = 2;
        if ( int( rand( 10 ) ) == 0 ) {
            $n = 4;
        }
        $board[$empty[$pos][0]][$empty[$pos][1]] = $n;
    }
}

sub invert {
    my @temp = ( [ 0, 0, 0, 0 ],
                 [ 0, 0, 0, 0 ],
                 [ 0, 0, 0, 0 ],
                 [ 0, 0, 0, 0 ] );
    for( my $r = 0; $r < 4; $r++ ) {
        for( my $c = 0; $c < 4; $c++ ) {
            $temp[$c][$r] = $board[$r][$c];
        }
    }
    @board = @temp;
}

sub mirror {
    my @temp = ( [ 0, 0, 0, 0 ],
                 [ 0, 0, 0, 0 ],
                 [ 0, 0, 0, 0 ],
                 [ 0, 0, 0, 0 ] );
    for( my $r = 0; $r < 4; $r++ ) {
        for( my $c = 0; $c < 4; $c++ ) {
            $temp[$r][3-$c] = $board[$r][$c];
        }
    }
    @board = @temp;
}

sub pushlt {
    my @temp = ( [ 0, 0, 0, 0 ],
                 [ 0, 0, 0, 0 ],
                 [ 0, 0, 0, 0 ],
                 [ 0, 0, 0, 0 ] );
    my $changed = 0;
    for( my $r = 0; $r < 4; $r++ ) {
        my $c_temp = 0;
        for( my $c = 0; $c < 4; $c++ ) {
            if( $board[$r][$c] != 0 ) {
                if( $temp[$r][$c_temp] == 0 ) {
                    $temp[$r][$c_temp] = $board[$r][$c];
                    $changed = 1;
                } elsif( $temp[$r][$c_temp] == $board[$r][$c] ) {
                    $score[0] = $score[0]+$temp[$r][$c_temp];
                    $temp[$r][$c_temp] = $temp[$r][$c_temp]*2;
                    $changed = 1;
                    $c_temp++;
                } else {
                    $c--;
                    $c_temp++;
                }
            }
        }
    }
    if ( $changed ) {
        $score[1]++;
        @board = @temp;
        addnum();
    }
}

sub pushrt {
    mirror();
    pushlt();
    mirror();
}

sub pushup {
    invert();
    pushlt();
    invert();
}

sub pushdn {
    invert();
    pushrt();
    invert();
}

main();
