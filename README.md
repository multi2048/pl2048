# 2048 in Perl
A language I've been thinking of since about 1999 when I first started messing
with web servers and getting aware of CGI scripts - which were often written in
Perl (though compiled applications in C were fairly common as well).

Haven't used it at all until now, and as such a perfect candidate for this
project.

Anyway, this is written for Perl 5. No idea if it works on Perl 6 or if the
changes are incompatible.
